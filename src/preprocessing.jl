function process_session_data(sessiondata)
  # Pokes
  preprocessingerrorflag = 0
  errortext  ="all seems well"
  pokesmouse = DataFrame()
  PokeIn                 = find((sessiondata[:, 1] .== LpokeIn) | (sessiondata[:,1] .== RpokeIn))
  PokeOut                = find((sessiondata[:,1] .== LpokeOut) | (sessiondata[:,1] .== RpokeOut))


  pokediff = length(PokeIn)-length(PokeOut)
  if pokediff > 0             #remove last codes when only poke in
    if pokediff>1
      errortext = "Unequal poke ins vs poke outs"
      preprocessingerrorflag = 1
      return pokesmouse, preprocessingerrorflag, errortext
    end
    #Correct for length and remove last poke in

    sessiondata                = sessiondata[1:(PokeIn[end]-1), :]
    PokeIn = PokeIn[1:end-1]
  end
  #AfterPokeCodes = sessiondata[PokeIn + 1, 1]
  # build sessiondataset
  #println(length(sessiondata[PokeIn, :x2]))
  #println(length(sessiondata[PokeOut, :x2]))
  pokesmouse[:TimeIn] = sessiondata[PokeIn, 2]
  pokesmouse[:TimeOut] = sessiondata[PokeOut, 2]
  pokesmouse[:Side]  = (sessiondata[PokeIn, 1] .== LpokeIn) #true is left!
  pokesmouse[:Durations] = pokesmouse[:TimeOut]-pokesmouse[:TimeIn]
  #pokesmouse[:Rewarded] = (AfterPokeCodes .== LpokeRew) | (AfterPokeCodes .== RpokeRew)
  #pokesmouse[:Omission] = (AfterPokeCodes .== LpokeOmission) | (AfterPokeCodes .== RpokeOmission)

  pokesmouse[:StreakNumber] = Array(Int64,length(PokeIn))
  pokesmouse[:Stim] = Array(Bool,length(PokeIn))
  pokesmouse[:SideHigh] = Array(Bool,length(PokeIn))
  pokesmouse[:Rewarded] = falses(length(PokeIn))
  pokesmouse[:Omission] = falses(length(PokeIn))


  if length(PokeIn) >=1
    pokesmouse[1,:StreakNumber] = 1
    pokesmouse[1, :Stim] = false
    pokesmouse[1, :SideHigh] = (sessiondata[3,1] == LeftHigh)
  else
    preprocessingerrorflag = 1
    errortext = "No pokes"
    return pokesmouse, preprocessingerrorflag, errortext
  end

  for i in 2:size(pokesmouse,1)
    inbetween = sessiondata[PokeIn[i-1]:PokeIn[i],1]
    if any(inbetween.== NewStreak)
      pokesmouse[i,:StreakNumber] = pokesmouse[i-1,:StreakNumber]+1
      pokesmouse[i,:Stim] = any(inbetween.== StimStreak)
    else
      pokesmouse[i,:StreakNumber] = pokesmouse[i-1,:StreakNumber]
      pokesmouse[i,:Stim] = pokesmouse[i-1,:Stim]
    end
    if any(inbetween.== LpokeRew) || any(inbetween.== RpokeRew)
      pokesmouse[i-1,:Rewarded] = true
    end
    if any(inbetween.== LpokeOmission) || any(inbetween.== RpokeOmission)
      pokesmouse[i-1,:Omission] = true
    end
    if any(inbetween.== LeftHigh)
      pokesmouse[i,:SideHigh] = true
    elseif any(inbetween.== RightHigh)
      pokesmouse[i,:SideHigh] = false
    else
      pokesmouse[i,:SideHigh] = pokesmouse[i-1,:SideHigh]
    end

  end

  #Some checks that the sessiondata is correctly processed
  if any(pokesmouse[:Durations] .< 0)
    errortext = "Negative Poking Time"
    preprocessingerrorflag = 1
  end

  pokesmouse = pokesmouse[pokesmouse[:Rewarded] | pokesmouse[:Omission], :]
  return pokesmouse, preprocessingerrorflag, errortext
end
