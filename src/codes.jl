# Everything with left port:
const LpokeRew            = 20;
const LpokeOmission       = 21;
const LpokeIn             = 22;
const LpokeOut            = 23;
const LRewStart           = 24;
const LRewStop            = 25;
const LpokeTooShort       = 26;
const LpokeTooFast        = 27;

# Everything with right port:
const RpokeRew            = 30;
const RpokeOmission       = 31;
const RpokeIn             = 32;
const RpokeOut            = 33;
const RRewStart           = 34;
const RRewStop            = 35;
const RpokeTooShort       = 36;
const RpokeTooFast        = 37;

#Current Probability Codes
const NewStreak           = 40;
const LeftHigh            = 41;
const LeftLow             = 42;
const RightHigh           = 43;
const RightLow            = 44;

#Block Codes
const NewBlock            = 50;
const LeftHighProbability = 51;
const LeftLowProbability  = 52;
const RightHighProbability = 53;
const RightLowProbability  = 54;
const Rewardsize          = 55;
const BlockNum            = 56;
const FlippingGamma       = 57;

#Stimulation
const StimStreak          = 61;
const NonStimStreak       = 62;
const StimOnset           = 63;
const StimOffset          = 64;
const StimPulsewidth      = 65;
const StimFreq            = 66;
const StimPower           = 67;

const StartingCode        = 98;
