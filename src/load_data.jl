function load_session_data(filepath)
  sessiondata = Array(Int64,0,2)
  errortext = "all seems well"
  preprocessingerrorflag = 0

  # Read in the data
  if ~isfile(filepath)
    errortext = "filepath does not exist"
    preprocessingerrorflag = 1
    return sessiondata, preprocessingerrorflag, errortext
  end

  try
    sessiondata = readdlm(filepath, '\t',Int64)

    #sessiondata = readtable(filepath, separator = ' ', header = false)
  catch
    # one file has blanks towards the end and I'm cutting it (C4_160707a.txt)!
    try
      sessiondata = readdlm(filepath, '\t')
      sessiondata = convert(Array{Int64,2},sessiondata[1:end-100,:])
    catch
      errortext = "Problem in Loading"
      preprocessingerrorflag = 1
      return sessiondata, preprocessingerrorflag, errortext
     end
  end
  return sessiondata, preprocessingerrorflag, errortext
end

function getpokes(gen_list)
  libraryname = "FlippingForagingDataLibrary.xlsx"
  foldername =  string("/Users/",ENV["USER"],"/Google Drive/Flipping/Task/Data/")
  dataLibrary = readxlsheet(string(foldername,libraryname), "Sheet1");

  pokestot = DataFrame()
  for gen in gen_list
    for session in 1:size(dataLibrary,1)
      filename = dataLibrary[session,1]
      if (dataLibrary[session,2] in gen.mice) && (dataLibrary[session,3] >= gen.firstday)
        filepath = string(foldername, filename)
        sessiondata, errorflag1, errortext1 = load_session_data(filepath)
        pokesmouse, errorflag2, errortext2 = process_session_data(sessiondata)
        if (errorflag1 == 1) || (errorflag2 == 1)
          println(errortext1)
          println(errortext2)
          println(filepath)
          continue
        end
        for j in 1:14
          pokesmouse[Symbol(dataLibrary[1,j])] = [dataLibrary[session,j] for i in 1:size(pokesmouse,1)]
        end
        pokesmouse[:Gen] = gen.name
        if isempty(pokestot)
          pokestot = copy(pokesmouse)
        else
          append!(pokestot, pokesmouse)
        end
      end
    end
  end
  pokestot[:DayNum] = copy(pokestot[:Date])
  pokestot[:ValidDay] = Array(Bool, size(pokestot,1))
  for gen in gen_list
    indexes = (pokestot[:Gen] .== gen.name)
    days = union(pokestot[indexes,:Date])
    sort!(days)
    for (ind,val) in enumerate(days)
      pokestot[(pokestot[:Date].==val) & indexes, :DayNum] = ind
    end
    pokestot[indexes,:ValidDay] = bitbroadcast(t -> t in gen.days, pokestot[indexes,:DayNum])
  end
  pokestot[:Protocol] = (pokestot[:FlippingGamma]+pokestot[:RewardProb])/60
  #pokestot[:Protocol] = broadcast((x,y) -> "$x -$y", pokestot[:RewardProb], pokestot[:FlippingGamma])
  pokestot[:Side] = [(t ? "Left":"Right") for t in (pokestot[:Side])]
  pokestot[:SideHigh] = [(t ? "Left":"Right") for t in (pokestot[:SideHigh])]
  return pokestot
end
