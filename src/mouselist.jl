type Gen
  name
  firstday
  mice
  days
end

const acc_wt = Gen("acc_wt", 160520, ["AC4";"AC5"], vcat(1:26))
const acc_h = Gen("acc_h",160520, ["AC1";"AC2";"AC3"], vcat(1:26))
const wt = Gen( "wt",
                160629,
                ["A1"; "A2"; "A3"; "A4"; "A5";
                "B1"; "B2"; "B3"; "B4"; "B5";
                "C1"; "C2"; "C3"; "C4"; "C5";
                "D1"; "D2"; "D3"; "D4"; "D5"],
                vcat(2:3,5:6,8:9,11:12,15:16,18:19,21:22,24:25))
const sert_h = Gen( "sert_h",
                    160911,
                    ["PC1";"PC2";"PC3";"PC4";"PC5";"PC6";"PC7"],
                    vcat(2:4,6:8,10:12,14:16))

const sert_wt = Gen("sert_wt",
                    161004,
                    ["PW1";"PW2";"PW3";"PW4";"PW5";"PW6"],
                    vcat(2:4,6:8,10:12,14:16))
